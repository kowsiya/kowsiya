import java.util.Scanner;

public class BankSystem {

	Scanner scan = new Scanner(System.in);

	double currentBalance = 0;
	int numberOfDeposit = 0;
	int numberOfWithdrawals = 0;

	public void Balance() {
		System.out.println("Welcome to ceylon Banking system");
		System.out.println("Enter your current balance : ");
		currentBalance = scan.nextDouble();
	}

	public void Deposit() {
		System.out.println("Enter the number of deposits (0 - 5) : ");
		numberOfDeposit = scan.nextInt();
	}

	public void Withdrawals() {
		System.out.println("Enter the number of withdrawals (0 - 5) : ");
		numberOfWithdrawals = scan.nextInt();
	}

	public void deposit() {
		for (int i = 1; i <= numberOfDeposit; i++) {
			System.out.println("Enter the amount of deposit #1");
			double amount = scan.nextDouble();

			if (amount <= 0) {
				System.out.println("***Deposit amount must be greater than zero, please re-enter. ");
				amount = scan.nextDouble();
			}
			currentBalance += amount;
		}
	}

	public void withdrawal() {
		for (int i = 1; i <= numberOfWithdrawals; i++) {

			System.out.println("Enter the amount of Withdrawal " + i);
			double amount = scan.nextDouble();
			if (amount > currentBalance) {
				System.out.println("***Withdrawal amount exceeds current  balance. ");
				amount = scan.nextDouble();
			}
			currentBalance -= amount;

		}
	}

}
