import java.util.Scanner;

public class PrimeNumbers {
	public static void main(String[] args) {

		int num1;
		boolean isPrime = true;
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome user");

		System.out.println("Enter a number : ");

		int num = scan.nextInt();
		for (int i = 2; i <= num / 2; i++) {
			num1 = num % i;

			if (num1 == 0) {
				isPrime = false;
				break;
			}
		}
		if (isPrime)
			System.out.println(num + " is a Prime Number ");
		else
			System.out.println(num + " is not a Prime Number");

		System.out.println("Bye Bye");
	}

}
